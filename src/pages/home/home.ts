import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { ToastController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor( private barcodeScanner: BarcodeScanner, private toastCtrl: ToastController ) {

  }

  scan(){

    console.log('realizando scan');
    this.barcodeScanner.scan().then((barcodeData) => {
      // Success! Barcode data is here
      console.log(barcodeData);
      
     }, (err) => {
         // An error occurred
        this.showError('Error: '+err);
               
     });

  }
  
  showError(message: string){
    
        let toast = this.toastCtrl.create({
          message: message,
          duration: 2500
        });
    
        toast.present();
      }

}
